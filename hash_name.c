#include <stdio.h>
#include <stdlib.h>

static inline unsigned long rol64(unsigned long word, unsigned int shift)
{
	return (word << (shift & 63)) | (word >> ((-shift) & 63));
}

#define HASH_MIX(x, y, a)	\
	(	x ^= (a),	\
	y ^= x, x = rol64(x,12),\
	x += y, y = rol64(y,45),\
	y *= 9			)

#define GOLDEN_RATIO_64	0x61C8864680B583EBull

unsigned long fold_hash(unsigned long x, unsigned long y)
{
	y ^= x * GOLDEN_RATIO_64;
	y *= GOLDEN_RATIO_64;
	return y >> 32;
}

// check each byte
// - if a byte is 0x00, it becomes non-zero(0x80)
// - if a byte isn't 0x00, it becomes zero(0x00)
static inline unsigned long has_zero(unsigned long a, unsigned long *bits)
{
	unsigned long one_bits = 0x0101010101010101ul;			// 0x00(NUL)
	unsigned long high_bits = 0x8080808080808080ul;			// ascii code < 0x80

	unsigned long mask = ((a - one_bits) & ~a) & high_bits;	// if 0x00, become 0xff, so caught by & 0x80
	*bits = mask;
	return mask;
}

static inline unsigned long create_zero_mask(unsigned long bits)
{
	printf("0x%lx\n", bits);
	bits = (bits - 1) & ~bits;
	printf("bits : 0x%lx\n", bits);
	return bits >> 7;
}

/*
 * [in] dentry pointer
 * [in] name
 * [out] hash
 */
int main(int argc, char *argv[])
{
	unsigned long a = 0, b;
	// [in] dentry pointer
	unsigned long x = 0, y = 0xffff9e6a4dfd9480;	// salt (parent dentry)
	
	// [in] name
	unsigned long round1 = 0x78742e3274736574; 		// test2.tx little endian
	unsigned long round2 = 0x0000000000000074;		// t little endian

	unsigned long adata, bdata, mask, len;
	unsigned long hash;

	a = round1;
	b = a ^ 0x2f2f2f2f2f2f2f2f;				// 575B011D5B5C4A5B
	has_zero(a, &adata);					// 0000000000000000
	has_zero(b, &bdata);					// 0000000000000000
	// if a don't have 0x00, or b has zero, HASH_MIX
	HASH_MIX(x, y, a);

	a = round2;
	b = a ^ 0x2f2f2f2f2f2f2f2f;				// 2F2F2F2F2F2F2F5B
	has_zero(a, &adata);					// 80808080808080'00'
	has_zero(b, &bdata);					// 0000000000000000
	// no HASH_MIX
	//HASH_MIX(x, y, a);

	// but a used anyway
	mask = create_zero_mask(adata|bdata);	// 8080808080807fff & 7f7f7f7f7f7f7f'ff' = 7fff
											// 7fff >> 7 = ff
	printf("adata : 0x%lx, bdata : 0x%lx, mask : 0x%lx\n", adata, bdata, mask);

	x ^= a & mask;

	hash = fold_hash(x, y);

	// [out] hash
	printf("x : 0x%lx, y : 0x%lx, hash : 0x%lx\n", x, y, hash);

	return 0;
}

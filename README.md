# Hash Name

## Purpose
- calculate hash manually
- CONFIG_DCACHE_WORD_ACCESS=y
- CONFIG_64BIT=Y

## Example
```
root@user-virtual-machine:~/hash_name# make
gcc -o hash_name hash_name.c -g

// parent dentry : 0xffff9e6a4dfd9480
// name : test2.txt
root@user-virtual-machine:~/hash_name# ./hash_name
0x8080808080808000
bits : 0x7fff
adata : 0x8080808080808000, bdata : 0x0, mask : 0xff
x : 0xca6ed79f6fe6390f, y : 0xd033187d266340b9, hash : 0xc748277
root@user-virtual-machine:~/hash_name#

// dentry's hash is 0xc748277
```
